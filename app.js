const app = angular.module('signupForm', []);

app.controller('formController', ['$scope', ($scope) => {
    $scope.namePattern = /^[a-zA-Z]+$/;
    $scope.passwordPattern = /^[a-zA-Z]\w{3,14}$/;
    $scope.isPasswordsMatched = (confirmPassword) => {
        $scope.ismatched = $scope.password == confirmPassword ? true : false;
    };  

    $scope.formSubmitted = () => window.location.reload();

}])